﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace ipmath
{
	public static class IPAddressExtensions
	{
		public static IPAddress Plus(this IPAddress current, int addNumber)
		{
			var currentBytes = current.GetAddressBytes();

			for (var count = 0; count < addNumber; count++)
			{
				currentBytes[3]++;

				for (var x = currentBytes.Length - 2; x >= 0; x--)
				{
					if (currentBytes[x + 1] == 0)
					{
						currentBytes[x]++;
					}
					else
					{
						break;
					}
				}
			}

			return new IPAddress(currentBytes);
		}

		public static bool ExistsBetween(this IPAddress subject, IPAddress lowerBounds, IPAddress upperBounds)
		{
			return subject.IsGreaterOrEqualTo(lowerBounds) && subject.IsLessOrEqualTo(upperBounds);
		}

		public static bool IsEqualTo(this IPAddress r1, IPAddress r2)
		{
			return r1 != null && r1.Equals(r2);
		}

		public static bool IsNotEqualTo(this IPAddress r1, IPAddress r2)
		{
			return !(r1 == r2);
		}

		public static bool IsLessThan(this IPAddress a, IPAddress b)
		{
			var aSide = a.GetAddressBytes();
			var bSide = b.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsGreaterThan(this IPAddress a, IPAddress b)
		{
			var aSide = a.GetAddressBytes();
			var bSide = b.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsGreaterOrEqualTo(this IPAddress r1, IPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					break;
				}
			}

			return true;
		}

		public static bool IsLessOrEqualTo(this IPAddress r1, IPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					break;
				}
			}

			return true;
		}
	}
}
