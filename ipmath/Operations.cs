﻿using System.Net;

namespace ipmath
{


	public static class IPMath
	{
		//Expecting format: 192.168.0.0/24
		public static IPAddress GetSubnetMask(string subnetBits)
		{
			var cidr = int.Parse(subnetBits);
			var zeroBits = 32 - cidr; // the number of zero bits
			var result = uint.MaxValue; // all ones

			// Shift "cidr" and subtract one to create "cidr" one bits;
			//  then move them left the number of zero bits.
			result &= (uint) ((((ulong) 0x1 << cidr) - 1) << zeroBits);

			// Note that the result is in host order, so we'd have to convert
			//  like this before passing to an IPAddress constructor
			return new IPAddress((uint) IPAddress.HostToNetworkOrder((int) result));
		}

		public static IPAddress GetMaxAddressOfSubnet(IPAddress networkAddress, IPAddress subnetMask)
		{
			var maxIPv4 = new IPAddress(0xFFFFFFFF);

			var inverseMask = new byte[maxIPv4.GetAddressBytes().Length];
			var maxIpv4Arr = maxIPv4.GetAddressBytes();
			var subnetArr = subnetMask.GetAddressBytes();

			for (var x = 0; x < inverseMask.Length; x++)
			{
				inverseMask[x] = (byte)(maxIpv4Arr[x] - subnetArr[x]);
			}

			var lastHostByteArr = networkAddress.GetAddressBytes();

			for (var x = 0; x < lastHostByteArr.Length; x++)
			{
				lastHostByteArr[x] = (byte)(lastHostByteArr[x] + inverseMask[x]);
			}

			return new IPAddress(lastHostByteArr);
		}

		public static bool AddressExistsWithin(IPAddress address, IPAddress networkAddress, IPAddress maxNetworkAddress)
		{
			//var addr = new ComparableIPAddress(address.GetAddressBytes());
			//var netw = new ComparableIPAddress(networkAddress.GetAddressBytes());
			//var bcast = new ComparableIPAddress(maxNetworkAddress.GetAddressBytes());

			//return addr >= netw && addr <= bcast;

			return address.ExistsBetween(networkAddress, maxNetworkAddress);
		}
	}
}