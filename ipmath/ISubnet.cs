﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace ipmath
{
	public interface ISubnet
	{
		IPAddress NetworkAddress { get; set; }
		IPAddress SubnetMask { get; set; }
	}

	public class Subnet : ISubnet
	{
		public IPAddress NetworkAddress { get; set; }
		public IPAddress SubnetMask { get; set; }
	}

	public static class SubnetExtensions
	{
		public static bool ConflictsWith(this ISubnet aSide, ISubnet bSide)
		{
			if (aSide.NetworkAddress.ExistsBetween(bSide.NetworkAddress, bSide.GetBroadcast()) ||
			    bSide.NetworkAddress.ExistsBetween(aSide.NetworkAddress, aSide.GetBroadcast()))
				return true;

			return false;
		}

		public static int TotalHosts(this ISubnet subnet)
		{
			var broadcastBytes = subnet.GetBroadcast().GetAddressBytes();
			var networkBytes = subnet.NetworkAddress.GetAddressBytes();

			var differenceArray = new byte[networkBytes.Length];

			for (var x = 0; x < networkBytes.Length; x++)
			{
				differenceArray[x] = (byte)(broadcastBytes[x] - networkBytes[x]);
			}

			return differenceArray.Where(x => x > 0).Aggregate(1, (current, curByte) => (current) * (curByte + 1)) - 2;
		}

		public static IPAddress GetNextAvailable(this ISubnet subnet, IEnumerable<IPAddress> usedAddresses)
		{
			var possibleAvailable = subnet.NetworkAddress;

			var broadcast = subnet.GetBroadcast();

			var usedList = usedAddresses.Select(x => x.ToString()).ToList();

			do
			{
				possibleAvailable = possibleAvailable.Plus(1);

				if (!usedList.Contains(possibleAvailable.ToString()))
					return possibleAvailable;

			} while (possibleAvailable.IsLessOrEqualTo(broadcast));

			return null;
		}

		public static IPAddress GetBroadcast(this ISubnet subnet)
		{
			var maxIPv4 = new IPAddress(0xFFFFFFFF);

			var inverseMask = new byte[maxIPv4.GetAddressBytes().Length];
			var maxIpv4Arr = maxIPv4.GetAddressBytes();
			var subnetArr = subnet.SubnetMask.GetAddressBytes();

			for (var x = 0; x < inverseMask.Length; x++)
			{
				inverseMask[x] = (byte)(maxIpv4Arr[x] - subnetArr[x]);
			}

			var lastHostByteArr = subnet.NetworkAddress.GetAddressBytes();

			for (var x = 0; x < lastHostByteArr.Length; x++)
			{
				lastHostByteArr[x] = (byte)(lastHostByteArr[x] + inverseMask[x]);
			}

			return new IPAddress(lastHostByteArr);
		}
	}
}
