﻿using System;
using System.Collections.Generic;
using System.Net;

namespace ipmath
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			//			var cidr = 23; // e.g., "/23"
			//			var zeroBits = 32 - cidr; // the number of zero bits
			//			var result = uint.MaxValue; // all ones
			//
			//			// Shift "cidr" and subtract one to create "cidr" one bits;
			//			//  then move them left the number of zero bits.
			//			result &= (uint)((((ulong)0x1 << cidr) - 1) << zeroBits);
			//
			//			// Note that the result is in host order, so we'd have to convert
			//			//  like this before passing to an IPAddress constructor
			//			result = (uint)IPAddress.HostToNetworkOrder((int)result);
			//
			//			Console.WriteLine(result);
			//
			//
			//			var ipobj = new IPAddress(result);
			//
			//			Console.WriteLine(ipobj.ToString());

			PrintSubnetAndMaxAddress("192.168.23.0","24");

			PrintSubnetAndMaxAddress("138.64.0.0","10");

			PrintSubnetAndMaxAddress("138.128.0.0", "10");


			PrintMaxAddress("121.192.0.0", "255.192.0.0");

			PrintConflictReport("121.128.0.0", "10", "121.128.64.0", "24");

			PrintGetNextAvailableReport();

			PrintTotalHostsFor("192.168.0.0", "24");

			PrintTotalHostsFor("192.168.128.0", "22");

			PrintTotalHostsFor("192.168.128.0", "18");

			PrintTotalHostsFor("10.128.0.0", "9");

			Console.ReadLine();
		}

		private static void PrintTotalHostsFor(string addr, string subBits)
		{
			var subnet = new Subnet { NetworkAddress = IPAddress.Parse(addr), SubnetMask = IPMath.GetSubnetMask(subBits) }; 

			Console.WriteLine("Total Hosts for " + subnet.NetworkAddress + "(" + subnet.SubnetMask + ") is " + subnet.TotalHosts());
		}

		private static void PrintGetNextAvailableReport()
		{
			var subnet = new Subnet { NetworkAddress = IPAddress.Parse("192.168.0.0"), SubnetMask = IPMath.GetSubnetMask("24") };

			var listOfUsed = new List<IPAddress>();

			listOfUsed.Add(IPAddress.Parse("192.168.0.1"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.2"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.3"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.4"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.5"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.7"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.8"));
			listOfUsed.Add(IPAddress.Parse("192.168.0.9"));

			var nextAvail = subnet.GetNextAvailable(listOfUsed);

			Console.WriteLine("Next available for " + subnet.NetworkAddress + " is " + nextAvail);
		}

		private static void PrintConflictReport(string firstAddr, string firstSubnetBits, string secondAddr, string secondSubnetBits)
		{
			var firstIP = IPAddress.Parse(firstAddr);
			var firstSubnetMask = IPMath.GetSubnetMask(firstSubnetBits);
			var firstMax = IPMath.GetMaxAddressOfSubnet(firstIP, firstSubnetMask);
			var secondIP = IPAddress.Parse(secondAddr);
			var secondSubnetMask = IPMath.GetSubnetMask(secondSubnetBits);
			var secondMax = IPMath.GetMaxAddressOfSubnet(secondIP, secondSubnetMask);

			if (IPMath.AddressExistsWithin(firstIP, secondIP, secondMax))
				Console.WriteLine(firstIP + " lies between " + secondIP + " and " + secondMax);
			else if (IPMath.AddressExistsWithin(secondIP, firstIP, firstMax))
				Console.WriteLine(secondIP + " lies between " + firstIP + " and " + firstMax);
			else
			{
				Console.WriteLine("IP ranges do not conflict.");
			}
		}

		private static void PrintMaxAddress(string networkAddress, string subnetMask)
		{
			var netAddr = IPAddress.Parse(networkAddress);
			var subAddr = IPAddress.Parse(subnetMask);
			
			var maxHost = IPMath.GetMaxAddressOfSubnet(netAddr, subAddr);

			Console.WriteLine("Max host of " + netAddr + " within " + subAddr + " is " + maxHost);
		}

		

		private static void PrintSubnetAndMaxAddress(string address, string subnetbits)
		{
			var networkAddress = IPAddress.Parse(address);

			var subnet =IPMath.GetSubnetMask(subnetbits);

			Console.WriteLine("NetworkAddress: " + networkAddress);
			Console.WriteLine("SubnetMask: " + subnet);


			var maxIPv4 = IPAddress.Parse("255.255.255.255");
			Console.WriteLine("Max overall address: " + maxIPv4);

			var byteArr = new byte[maxIPv4.GetAddressBytes().Length];
			var maxArr = maxIPv4.GetAddressBytes();
			var subArr = subnet.GetAddressBytes();

			for (var x = 0; x < byteArr.Length; x++)
			{
				byteArr[x] = byte.Parse((maxArr[x] - subArr[x]).ToString());
			}

			var result = new IPAddress(byteArr);
			
			Console.WriteLine("Max minus subnet: " + result);

			var lastHost = networkAddress.GetAddressBytes();

			for (var x = 0; x < lastHost.Length; x++)
			{
				lastHost[x] = byte.Parse((networkAddress.GetAddressBytes()[x] + result.GetAddressBytes()[x]).ToString());
			}

			Console.WriteLine("Last Host: " + (new IPAddress(lastHost)));
		}
	}

//		public static byte[] MaxAddress(this ISubnet subnet)
//		{
//			var addressBytes = subnet.NetworkAddress.GetAddressBytes();
//			var subnetBytes = subnet.SubnetMask.GetAddressBytes();
//
//			var allOnes = byte.Parse("255");
//
//
//		}

	
		
}
