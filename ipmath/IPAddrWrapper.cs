﻿using System.Net;

namespace ipmath
{
	public class IPAddrWrapper : IPAddress
	{
		protected bool Equals(IPAddrWrapper other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((IPAddrWrapper) obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public IPAddrWrapper(long newAddress) : base(newAddress)
		{
		}

		public IPAddrWrapper(byte[] address, long scopeid) : base(address, scopeid)
		{
		}

		public IPAddrWrapper(byte[] address) : base(address)
		{
		}

		// Omitting any of the following operator overloads 
		// violates rule: OverrideMethodsOnComparableTypes.
		public static bool operator ==(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			return r1 != null && r1.Equals(r2);
		}
		public static bool operator !=(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			return !(r1 == r2);
		}
		public static bool operator <(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}
		public static bool operator >(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}

		public static bool operator >=(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					break;
				}
			}

			return true;
		}

		public static bool operator <=(IPAddrWrapper r1, IPAddrWrapper r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					break;
				}
			}

			return true;
		}
	}
}