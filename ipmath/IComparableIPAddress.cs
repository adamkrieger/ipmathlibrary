﻿using System;
using System.Net;

namespace ipmath
{
	//public interface IComparableIPAddress : IComparable
	//{
	//	 IPAddress 
	//}

	public class ComparableIPAddress : IPAddress//, IComparableIPAddress
	{
		protected bool Equals(ComparableIPAddress other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((ComparableIPAddress) obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public ComparableIPAddress(long newAddress) : base(newAddress)
		{
		}

		public ComparableIPAddress(byte[] address, long scopeid) : base(address, scopeid)
		{
		}

		public ComparableIPAddress(byte[] address) : base(address)
		{
		}

		// Omitting any of the following operator overloads 
		// violates rule: OverrideMethodsOnComparableTypes.
		public static bool operator ==(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			return r1 != null && r1.Equals(r2);
		}
		public static bool operator !=(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			return !(r1 == r2);
		}
		public static bool operator <(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}
		public static bool operator >(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					return true;
				}

				if (x == (aSide.Length - 1))
				{
					return false;
				}
			}

			return true;
		}

		public static bool operator >=(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] < bSide[x])
				{
					return false;
				}

				if (aSide[x] > bSide[x])
				{
					break;
				}
			}

			return true;
		}

		public static bool operator <=(ComparableIPAddress r1, ComparableIPAddress r2)
		{
			var aSide = r1.GetAddressBytes();
			var bSide = r2.GetAddressBytes();

			for (var x = 0; x < aSide.Length; x++)
			{
				if (aSide[x] > bSide[x])
				{
					return false;
				}

				if (aSide[x] < bSide[x])
				{
					break;
				}
			}

			return true;
		}

		public int CompareTo(object obj)
		{
			if(obj==null)return 1;

			var objCasted = obj as ComparableIPAddress;

			if(objCasted==null)
			{
				throw new ArgumentException();
			}

			return this.ToString().CompareTo(objCasted.ToString());
		}
	}
}